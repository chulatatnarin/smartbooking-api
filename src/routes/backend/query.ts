import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { CustomersModel } from '../../models/booking/customers';

import { ReservesModel } from '../../models/booking/reserves';
import { ServicesModel } from '../../models/booking/services';
import { HospitalsModel } from '../../models/booking/hospitals';
import { PeriodsModel } from '../../models/booking/periods';
import { UsersModel } from '../../models/booking/users';
import { ServiceTypesModel } from '../../models/booking/service_types';
import { SlotsModel } from '../../models/booking/slots';
import { ProfilesModel } from '../../models/booking/profiles';
import S from 'fluent-json-schema';


export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const customersModel = new CustomersModel();
  const reservesModel = new ReservesModel();
  const servicesModel = new ServicesModel();
  const hospitalsModel = new HospitalsModel();
  const periodsModel = new PeriodsModel();
  const usersModel = new UsersModel();
  const serviceTypesModel = new ServiceTypesModel();
  const slotsModel = new SlotsModel();
  const profilesModel = new ProfilesModel();

  fastify.get('/customers/list', async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      let datas: any = await customersModel.list(db);

      return reply.status(StatusCodes.CREATED)
        .send({
          status: StatusCodes.CREATED,
          ok: true,
          results: datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.post('/getManagementSlot', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }
    // let datas :any
    let slots: any
    let slot: any
    let reserves: any
    let reserve: any
    let periods: any
    let period: any
    let service_types: any
    let service_type: any
    let hospitals: any
    let hospital: any
    let profiles: any
    let profile: any
    let customers: any
    let customer: any

    try {

      // data = {
      //slot_date
      //hospital_id
      // }
      slots = await slotsModel.getManagementSlot(db, data);

      if (slots[0]) {
        slot = slots[0];
        reserves = await reservesModel.getBySlotID(db, slot.slot_id);
        if (reserves[0]) {
          reserve = reserves[0];
          profiles = await profilesModel.getByID(db, reserve.user_id);
          if (profiles[0]) {
            profile = profiles[0];
            reserve.profile = profile;
          }
          customers = await customersModel.getByID(db, reserve.customer_id);
          if (customers[0]) {
            customer = customers[0];
            reserve.customer = customer;
          }
        }
        periods = await periodsModel.getByID(db, slot.period_id);
        if (periods[0]) {
          period = periods[0];
        }
        service_types = await serviceTypesModel.getByID(db, slot.service_type_id);
        if (service_types[0]) {
          service_type = service_types[0];
        }
        hospitals = await hospitalsModel.getByID(db, slot.hospital_id);
        if (hospitals[0]) {
          hospital = hospitals[0];
        }

      }
      let info = slot;
      info.reserve = reserve;
      info.period = period;
      info.service_type = service_type;
      info.hospital = hospital;

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: info
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.post('/getManagementReserve', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }
    // let datas :any
    let slots: any
    let slot: any
    let reserves: any
    let reserve: any
    let periods: any
    let period: any
    let service_types: any
    let service_type: any
    let hospitals: any
    let hospital: any
    let profiles: any
    let profile: any
    let customers: any
    let customer: any

    try {

      // data = {
      //reserve_date
      // }
      reserves = await reservesModel.getManagementReserve(db, data);
      console.log(reserves);

      if (reserves[0]) {
        reserve = reserves[0];
        profiles = await profilesModel.getByID(db, reserve.user_id);
        if (profiles[0]) {
          profile = profiles[0];
          reserve.profile = profile;
        }
        customers = await customersModel.getByID(db, reserve.customer_id);

        if (customers[0]) {
          customer = customers[0];
          reserve.customer = customer;
        }

        slots = await slotsModel.getByID(db, reserve.slot_id);

        if (slots[0]) {
          slot = slots[0];
          periods = await periodsModel.getByID(db, slot.period_id);
          if (periods[0]) {
            period = periods[0];
            period = period;
          }
          service_types = await serviceTypesModel.getByID(db, slot.service_type_id);
          if (service_types[0]) {
            service_type = service_types[0];
            service_type = service_type;
          }
          hospitals = await hospitalsModel.getByID(db, slot.hospital_id);
          if (hospitals[0]) {
            hospital = hospitals[0];
            hospital = service_type
          }

        }

      }
      let info = slot;
      info.reserve = reserve;
      info.period = period;
      info.service_type = service_type;
      info.hospital = hospital;

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: info
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.post('/getManagementCustomer', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }
    // let datas :any
    let slots: any
    let slot: any
    let reserves: any
    let reserve: any
    let periods: any
    let period: any
    let service_types: any
    let service_type: any
    let hospitals: any
    let hospital: any
    let profiles: any
    let profile: any
    let customers: any
    let customer: any

    try {

      // data = {
      //searchtext
      // }
      customers = await customersModel.getSearch(db, data.searchtext);

      if (customers[0]) {
        customer = customers[0];

        // reserve.customer = customer;
        reserves = await reservesModel.getByCustomerID(db, customer.customer_id);

        if (reserves[0]) {
          reserve = reserves[0];
          profiles = await profilesModel.getByID(db, reserve.user_id);
          console.log('profiles : ', profiles);

          if (profiles[0]) {
            profile = profiles[0];
            reserve.profile = profile;
          }

          slots = await slotsModel.getByID(db, reserve.slot_id);
          if (slots[0]) {
            slot = slots[0];
            periods = await periodsModel.getByID(db, slot.period_id);
            if (periods[0]) {
              period = periods[0];
              period = period;
            }
            service_types = await serviceTypesModel.getByID(db, slot.service_type_id);
            if (service_types[0]) {
              service_type = service_types[0];
              service_type = service_type;
            }
            hospitals = await hospitalsModel.getByID(db, slot.hospital_id);
            if (hospitals[0]) {
              hospital = hospitals[0];
              hospital = service_type
            }

          }

        }

      }

      let info = slot;
      info.reserve = reserve;
      info.period = period;
      info.service_type = service_type;
      info.hospital = hospital;

      return reply.status(StatusCodes.OK)
        .send({
          status: StatusCodes.OK,
          ok: true,
          results: info
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  done();
}