import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { CustomersModel } from '../../models/booking/customers';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const customersModel = new CustomersModel();

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      let datas :any = await customersModel.list(db);

      return reply.status(StatusCodes.OK)
        .send({
          status:StatusCodes.OK,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  done();
}