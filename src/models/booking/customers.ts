import { Knex } from 'knex';

export class CustomersModel {

  list(db: Knex) {
    return db('customers');
  }  

  getByID(db: Knex, id: number) {
    return db('customers')
    .where('customer_id', id);
  }  

  getSearch(db: Knex, text: string) {
    return db('customers')
    .whereLike('customer_name', `%${text}%`);
  }  

  create(db: Knex, data: any) {
    return db('customers')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('customers')
    .where('customer_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('customers')
      .where('customer_id', id)
      .delete();
  }

}