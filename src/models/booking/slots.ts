import { Knex } from 'knex';

export class SlotsModel {

  list(db: Knex) {
    return db('slots');
  }

  getByID(db: Knex, id: number) {
    return db('slots')
      .where('slot_id', id)
      .andWhere('is_active', true);
  }

  getByPeriodID(db: Knex, id: number) {
    return db('slots')
      .where('period_id', id)
      .andWhere('is_active', true);
  }

  getByServiceID(db: Knex, id: number) {
    return db('slots')
      .where('service_id', id)
      .andWhere('is_active', true);
  }

  getByServiceTypeID(db: Knex, id: number) {
    return db('slots')
      .where('service_type_id', id)
      .andWhere('is_active', true);
  }

  getSearch(db: Knex, text: string) {
    return db('slots')
      .whereLike('slot_name', `%${text}%`);
  }

  create(db: Knex, data: any) {
    return db('slots')
      .insert(data)
      .returning('*');
  }

  update(db: Knex, data: any, id: number) {
    return db('slots')
      .where('slot_id', id)
      .update(data)
      .returning('*');
  }

  delete(db: Knex, id: number) {
    return db('slots')
      .where('slot_id', id)
      .delete();
  }

  getManagementSlot(db: Knex, data: any) {
    return db('slots')
      .where('slot_date', data.slot_date)
      .andWhere('hospital_id', data.hospital_id)
      .andWhere('is_active', true);
  }
}