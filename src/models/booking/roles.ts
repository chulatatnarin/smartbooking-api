import { Knex } from 'knex';

export class RolesModel {

  list(db: Knex) {
    return db('roles');
  }  

  getByID(db: Knex, id: any) {
    console.log(id);
    
    return db('roles')
    .where('role_id', id)
    .andWhere('is_active',true);
  }  

  getSearch(db: Knex, text: string) {
    return db('roles')
    .whereLike('role_name', `%${text}%`)
    .andWhere('is_active',true);
  }  

  create(db: Knex, data: any) {
    return db('roles')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: any) {
    return db('roles')
    .where('role_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: any) {
    return db('roles')
      .where('role_id', id)
      .delete();
  }

}