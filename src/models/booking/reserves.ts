import { Knex } from 'knex';

export class ReservesModel {

  list(db: Knex) {
    return db('reserves');
  }

  getByID(db: Knex, id: number) {
    return db('reserves')
      .where('reserve_id', id);
  }

  getByCustomerID(db: Knex, id: number) {
    return db('reserves')
      .where('customer_id', id);
  }

  getBySlotID(db: Knex, id: number) {
    return db('reserves')
      .where('slot_id', id);
  }

  getByUserID(db: Knex, id: number) {
    return db('reserves')
      .where('user_id', id);
  }

  getByServiceTypeID(db: Knex, id: number) {
    return db('reserves')
      .where('service_type_id', id);
  }
  getSearch(db: Knex, text: string) {
    return db('reserves')
      .whereLike('reserve_note', `%${text}%`);
  }

  create(db: Knex, data: any) {
    return db('reserves')
      .insert(data)
      .returning('*');
  }

  update(db: Knex, data: any, id: number) {
    return db('reserves')
      .where('reserve_id', id)
      .update(data)
      .returning('*');
  }

  delete(db: Knex, id: number) {
    return db('reserves')
      .where('reserve_id', id)
      .delete();
  }

  getManagementReserve(db: Knex, data: any) {
    console.log(data);

    return db('reserves')
      .where('reserve_date', data.reserve_date)
  }
}