import { Knex } from 'knex';

export class HospitalsModel {

  list(db: Knex) {
    return db('hospitals');
  }  

  info(db: Knex) {
    return db('hospitals')
    .where('is_active', true);
  }  

  getByID(db: Knex, id: any) {
    console.log(id);
    
    return db('hospitals')
    .where('hospital_id', id);
  }  

  getByHCode(db: Knex, id: string) {
    return db('hospitals')
    .where('hospital_code', id);
  }  

  getSearch(db: Knex, text: string) {
    return db('hospitals')
    .whereLike('hospital_name', `%${text}%`);
  }  

  create(db: Knex, data: any) {
    return db('hospitals')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: any) {
    return db('hospitals')
    .where('hospital_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: any) {
    return db('hospitals')
      .where('hospital_id', id)
      .delete();
  }

}