import { Knex } from 'knex';

export class ServicesModel {

  list(db: Knex) {
    return db('services');
  }  

  getByID(db: Knex, id: number) {
    return db('services')
    .where('service_id', id)
    .andWhere('is_active',true);
  }  

  getSearch(db: Knex, text: string) {
    return db('services')
    .whereLike('service_name', `%${text}%`)
    .andWhere('is_active',true);
  }  

  create(db: Knex, data: any) {
    return db('services')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('services')
    .where('service_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('services')
      .where('service_id', id)
      .delete();
  }

}